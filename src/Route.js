import React from 'react';
import { Switch, Link, BrowserRouter, Route } from "react-router-dom";
import CommentDetails from './components/app1/CommentDetails';
import Weather from './components/app2/Weather';
import Dashboard from './components/app3/Pics';
import Video from './components/app4/Video';
import Header from './components/commons/Header';

class Router extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path="/">
                            <CommentDetails />
                        </Route>
                        <Route path="/weather">
                            <Weather />
                        </Route>
                        <Route path="/pics">
                            <Dashboard />
                        </Route>
                        <Route path="/videos">
                            <Video />
                        </Route>
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

export default Router;