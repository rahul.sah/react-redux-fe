import React from 'react';
import './App.css';
import Router from './Route';
function App() {
  return (
    <div>
      <Router/>
    </div>
  );
}

export default App;
