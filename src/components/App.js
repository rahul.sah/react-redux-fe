import React from 'react';
import Clock from './Clock';

const App = () => {
    const formLabel = { name: "Enter Name:" };
    const buttonText = { text: 'Click Me' };
    const style = { backgroundColor: 'blue', color: 'white' };
    return (
        <div>
            <Clock />
            <form>
                <label className="label" htmlFor="name">{formLabel.name}</label>
                <input id="name" type="text" />
                <button style={style}>{buttonText.text}</button>
            </form>
        </div>
    )
}

export default App;
