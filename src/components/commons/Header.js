import React from 'react';
import {
    Link,BrowserRouter as Router
} from "react-router-dom";



const Header = (props) => {
    return (
        <div className="ui secondary  menu">
            <a className="active item">
                <Link to="/">Comments App</Link>
            </a>
            <a className="item">
                <Link to="/weather">Weather App</Link>
            </a>
            <a className="item">
                <Link to="/pics">Pics App</Link>
            </a>
            <a  className="item">
                <Link to="/videos">Videos</Link>
            </a>
            <div className="right menu">
                <div className="item">
                    <div className="ui icon input">
                        <input type="text" placeholder="Search..." />
                        <i className="search link icon"></i>
                    </div>
                </div>
                <a className="ui item">
                    Logout
      </a>
            </div>
        </div>
    );
}

export default Header;