import React from 'react';
import SeasonDisplay from './SeasonDisplay';
import Spinner from '../commons/Spinner';
class Weather extends React.Component {
    constructor(props) {
        super(props);
        this.state = { lat: null, errorMessage: '' };

    }
    componentDidMount() {
        console.log("My component was rendered on the screen");
        window.navigator.geolocation.getCurrentPosition(
            position => this.setState({ lat: position.coords.latitude }),
            err => this.setState({ errorMessage: err.message }));
    }

    componentDidUpdate() {
        console.log("My component was just updated");
    }

    componentWillUnmount() {
        console.log("Component will unmount");
    }

    render() {
        if (this.state.errorMessage && !this.state.lat) {
            return <div>Error: + {this.state.errorMessage} + </div>;
        }
        if (!this.state.errorMessage && this.state.lat) {
            return <SeasonDisplay lat={this.state.lat}/>;
        }
        return <Spinner message="Please except location request"/>
    }
}

export default Weather;