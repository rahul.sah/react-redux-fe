import React from 'react';
import faker from 'faker';
import Comment from './Comment';
import ApprovalCard from './ApprovalCard';

const Comments = (props) => {
    return (
        <div className="ui container comments">
            <h3 className="ui dividing header">Comments</h3>
            <ApprovalCard>
                <Comment image={faker.image.avatar()} desc="How Lovely!!" author={faker.internet.userName()} timeAgo={new Date().toLocaleDateString()} />
            </ApprovalCard>
            <ApprovalCard>
                <Comment image={faker.image.avatar()} desc="How Sexy!!!" author={faker.internet.userName()} timeAgo={new Date().toLocaleDateString()} />
            </ApprovalCard>
            <ApprovalCard>
                <Comment image={faker.image.avatar()} desc="How Real!!" author={faker.internet.userName()} timeAgo={new Date().toLocaleDateString()} />
            </ApprovalCard>

        </div>)
}

export default Comments;