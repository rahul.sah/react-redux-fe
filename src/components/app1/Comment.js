import React from 'react';

const Comment = (props) => {
    return (
        <div className="comment">
            <div className="avatar"><img alt="avatar" src={props.image} /></div>
            <div className="content">
                <a href="#" className="author">{props.author}</a>
                <div className="metadata"><div>{props.timeAgo}</div></div>
                <div className="text">{props.desc}</div>
                <div className="actions"><a href="#" className="">Reply</a></div>
            </div>
        </div>
    );
}

export default Comment;