import axios from 'axios';

export default axios.create({
    baseURL: "https://api.unsplash.com",
    headers: {
        Authorization: 'Client-ID CuYOYTdGA7v3kE7YHIi__6RxP4gRK4adubYNZ3Z72oE'
    }
})