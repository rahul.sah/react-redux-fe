import React from 'react';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        console.log("props");
        this.state = { currentTime: "" };
    }
    format = (format) => {
        if (isNaN(format)) {
            return false;
        }
        if (format < 10) {
            format = "0" + format;
        }
        return format;
    }

    clock = () => {
        setTimeout(() => {
            var today = new Date(),
                hours = today.getHours(),
                minutes = today.getMinutes(),
                seconds = today.getSeconds();
            var time = this.format(hours) + ":" + this.format(minutes) + ":" + this.format(seconds);
            this.setState({ currentTime: time });
        }, 1000);
    };
    render() {
        this.clock();
        return (<div><span>{this.state.currentTime ? this.state.currentTime : "Loading"}</span></div>);
    }
}

export default Clock;