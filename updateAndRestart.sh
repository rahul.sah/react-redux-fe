# install the create-react-app depdencies
pm2 stop server.js
# Delete the old repo
echo "Entered the ec2 instance"
sudo rm -rf /home/ec2-user/react-redux-fe/
# clone the repo again
git clone https://gitlab.com/rahul.sah/react-redux-fe.git
cd /home/ec2-user/react-redux-fe
npm i create-react-app
npm install
npm run build
pm2 start server.js